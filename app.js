var express = require('express');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cors=require('cors');
const MongoClient = require('mongodb').MongoClient;
var gitRoutes=require('./routes/github');
var app = express();

// // Use connect method to connect to the server
// MongoClient.connect(url, function(err, client) {
//     assert.equal(null, err);
//     console.log("Connected successfully to server");
//     db = client.db('mydb');  
// });


app.use(cors());
app.use(logger('dev'));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: false }));
app.use(cookieParser());

app.set('port', (process.env.PORT || 4000));
app.listen(app.get('port'), function() {
    console.log('Node app is running on port: ', app.get('port'));
});

app.use('/git',gitRoutes);
