var gh = require('../config/gitConfig');
var assert = require('assert');
var MongoClient = require('mongodb').MongoClient;

const url = 'mongodb://localhost:27017';
var db;

// Use connect method to connect to the server
MongoClient.connect(url, function(err, client) {
    assert.equal(null, err);
    console.log("Connected successfully to server");
    db = client.db('mydb');  
});



var dbOperations={
getRepo:function(name, callback){
    console.log(name);
    db.collection("github").find({name:name}, { projection: { _id:0 } }).toArray(function(err, result) {
      if (err) throw err;
      else{
          if(result.length){
            return callback(err,result);
          }
          else{
            var user = gh.getUser(name);
            user.listRepos(function(err, repos) {

            var insData = { name: name, data: repos };
            db.collection("github").insertOne(insData, function(err, res) {
                if (err) throw err;
                console.log("1 document inserted");
                return callback(err, insData);
            });
          });
        }
      }  
    });
},

starRepo:function(data,callback){
    var repo = gh.getRepo(data.name, data.repo);
    if(!data.star){
        repo.star(function(err,res){
            if(err)throw err;
            return callback(err,res)
        });
    } else {
        repo.unstar(function(err,res){
            if(err)throw err;
            return callback(err,res)
        })
    }
}
};
module.exports=dbOperations;