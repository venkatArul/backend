var MongoClient = require('mongodb').MongoClient;

module.exports = function(params) {

  var ip = params.ip || process.env.IP;
  var port = params.port || 27017;
  var collection = params.collection;

  var connection = MongoClient.connect('mongodb://' + 'localhost' + ':' + port + '/' + collection);
  connection
  .then(db => {
      console.log('success')
      return db;
  })


  return connection;

}