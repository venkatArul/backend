var express = require('express');
var router = express.Router();
var db=require('../model/dbOperation');

router.post('/getRepo/',function(req,res,next){
  db.getRepo(req.body.name,function(err,result){
    if(err)res.json(err);
    else{
        console.log(result);
        res.send(JSON.stringify(result));
    }
    });
});

router.post('/star/',function(req,res,next){
    db.starRepo(req.body, function(err,result){
        if(err)res.json(err);
        else{
            console.log(result);
            res.send({success:"success"})
        }
    })
});

module.exports = router;